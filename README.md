# k8s-pine64

`k8s-pine64` will help you to setup a Kubernetes cluster taking care of all digital activities:

- configuration of the operating system
- configuration of external storage (sdcard, usb disk)
- installation of the cluster
  - the master node
  - the worker nodes
- installation of services
  - the network module `flannel`
  - the `Kubernetes dashboard`
  - the load balancer `metallb`
  - the ingress `traefik`
  - monitoring services with `influxdb`, `heapster` and `grafana`

# 1. Inventory

Edit and update the file [inventory] according to your naming conventions, network configuration and of course
your cluster size.

# 2. Customization

Edit and update the file [group_vars/all.yml] according to your needs.

# 3. Generate the system images

Once the [inventory] is completed, the images of each k8s' node can be generated.
The generation is handled by the playbook [images.yml].

```bash
ansible-playbook images.yml
```

At the end of the execution you will have in the `playbook_dir` folder, one `.img` file by k8s' node.

The `.img` files should already be configured with:
- configured to start on EMMC or not
- the right hostname
- the right IP
- the right dns server

Now, the images can be burned to the sdcard/EMMC and then plug to boards.
Finally boards can be started.

# 4. Start cluster installation

Once all boards are ready, the cluster installation can be started.
The installation is handled by the playbook [cluster-install.yml].

```bash
ansible-playbook cluster-install.yml
```

[inventory]: ./inventory
[images.yml]: ./images.yml
[cluster-install.yml]: ./cluster-install.yml
[group_vars/all.yml]: ./group_vars/all.yml